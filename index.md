# Home
Aplicaciones de la Matemática en la Ingeniería.

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/mat281_2021), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/mat281_2021
```

## Contenidos

```{tableofcontents}
```